import {test, expect} from "../src/helper";
import {ENVIRONMENT} from "../src/environments";

const requestBody = {
    "jsonrpc": "2.0",
    "id": "healthcheck",
    "method": "getmininginfo",
    "params": []
}
test.describe("Send request via shared endpoint", () => {
    test.beforeEach(async ({ mainPage, signInPage, signInPageWithEmail})=>{
        await mainPage.goTo();
        await mainPage.goToAccount();
        await signInPage.clickSignInWithEmail();
        await signInPageWithEmail.loginByEmail(ENVIRONMENT.USER_NAME, ENVIRONMENT.PASSWORD);
    })
    test.afterEach(async ({accountPage})=>{
        await accountPage.removeEndpoint();
    })
    test('request sent successfully', async ({webAction, accountPage}) => {
        const endpoint = await accountPage.getEndpoint('Bitcoin','Testnet');
        const response = await webAction.sendRequest(endpoint, requestBody)
        await expect(response).toBeOK();
    });
})

