# Test task
## Run local
```shell
docker build -f .docker/Dockerfile.base -t test-task:lates .
docker run -e USER_NAME=${USER_NAME} -e PASSWORD=${PASSWORD} -v $(pwd):/app test-task:lates npm run test:smoke
```
