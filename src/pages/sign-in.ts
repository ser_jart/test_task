import {Page} from '@playwright/test';

export class SignInPage {
    constructor(public readonly page: Page) {
    }

    async clickSignInWithEmail(): Promise<void> {
        await this.page.getByRole('link', { name: 'Email' }).click();
    }
}