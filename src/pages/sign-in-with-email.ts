import {Page} from "@playwright/test";

export class SignInWithEmailPage {
    constructor(public readonly page: Page) {
    }

    async loginByEmail(email, password): Promise<void> {
        await this.page.getByRole('textbox', {name: 'Email'}).fill(email);
        await this.page.getByPlaceholder('Password').fill(password);
        await this.page.getByText('Continue', {exact: true}).click();
        await this.page.waitForURL('https://account.getblock.io');
    }
}