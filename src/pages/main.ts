import {Page} from "@playwright/test";

export class MainPage {
    constructor(public readonly page: Page) {
    }

    async goTo(): Promise<void> {
        await this.page.goto('', {waitUntil: 'networkidle'});
    }

    async goToAccount(): Promise<void> {
        await this.page.getByRole('link', {name: 'Account', exact: true}).click();
    }
}