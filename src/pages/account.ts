import {Page} from "@playwright/test";
import {expect} from "../helper";

export class AccountPage{
    constructor(public readonly page: Page) {
    }

    async getEndpoint(protocol,network): Promise<string>{
        await this.page.getByPlaceholder('Search Protocols').click();
        await this.page.locator('.dropdown-item').getByText( protocol,{exact:true}).click();
        await this.page.getByText('Select Network').click();
        await this.page.locator('.dropdown-item').filter({ hasText: network}).click();
        await this.page.getByRole('button',{ name: 'Get'}).last().click();
        await this.page.getByRole('button', { name: 'Copy' }).first().click();

        return  await this.page.locator('//*[contains(text(),"My endpoints")]/..//input').inputValue();
    }

    async removeEndpoint(): Promise<void>{
        await this.page.getByRole('button', { name: 'dots-vertical' }).last().click();
        await this.page.getByText('Remove from list').click();
        await expect(await this.page.locator('//h2[contains(text(),"My endpoints")]/../div/div')).toHaveCount(0);
    }
}