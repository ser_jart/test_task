import {test as base} from '@playwright/test';
import {AccountPage} from "./pages/account";
import {SignInPage} from "./pages/sign-in";
import {SignInWithEmailPage} from "./pages/sign-in-with-email";
import {MainPage} from "./pages/main";
import {WebAction} from "./WebAction";

export const test = base.extend<{
    webAction: WebAction;
    mainPage: MainPage;
    accountPage: AccountPage;
    signInPage: SignInPage;
    signInPageWithEmail: SignInWithEmailPage;
}>({
    mainPage: async ({page}, use) => {
        await use(new MainPage(page));
    },
    webAction: async ({page}, use) => {
        await use(new WebAction(page));
    },
    accountPage: async ({page}, use) => {
        await use(new AccountPage(page));
    },
    signInPage: async ({page}, use) => {
        await use(new SignInPage(page));
    },
    signInPageWithEmail: async ({page}, use) => {
        await use(new SignInWithEmailPage(page));
    },
})
export {expect} from '@playwright/test'
