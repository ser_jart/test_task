import 'dotenv/config'

interface baseEnvironment {
    USER_NAME: string;
    PASSWORD: string;
}

export const ENVIRONMENT: baseEnvironment = {
    USER_NAME: process.env.USER_NAME,
    PASSWORD: process.env.PASSWORD,
}
