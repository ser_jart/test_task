import {APIResponse, Page, request} from "@playwright/test";

export class WebAction {
    constructor(readonly page: Page) {
    }

    async sendRequest(url, body): Promise<APIResponse> {
        const context = await request.newContext();
        return await context.post(url, {data: body});
    }
}